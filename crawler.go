package main

import (
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"os"
	"sync"
)

// Crawler struct manage requests
// and return responses
type Crawler struct {
	Request      Request
	MaxPages     int
	TotalItems   int
	ItemsPerPage int
	UniqueID     string
}

// CreateCrawler func create basic
// crawler with attached first request
func CreateCrawler(req Request) (Crawler, error) {
	httpClient := http.Client{}
	res, _, err := req.SendRequest(httpClient)

	if err != nil {
		return Crawler{}, err
	}

	maxPages := float64(res.TotalItems()) / float64(req.ItemsPerPage())

	return Crawler{
		Request:      req,
		ItemsPerPage: req.ItemsPerPage(),
		TotalItems:   res.TotalItems(),
		MaxPages:     int(math.Ceil(maxPages)),
		UniqueID:     GenerateRandomString(5),
	}, nil
}

// DownloadImage func fetch image from URL
// and save to file given in pathToFile parameter
// (always recreate file!)
func DownloadImage(image ProductImage, httpClient http.Client, pathToFile string) error {
	req, err := http.NewRequest(http.MethodGet, image.GetOriginalURL(), nil)
	req.Close = true
	if err != nil {
		return err
	}

	res, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	file, err := os.Create(pathToFile)
	defer file.Close()
	if err != nil {
		return err
	}

	_, err = io.Copy(file, res.Body)

	return nil
}

// DownloadWorker func iterates over all created quests
// and return products to another channel
func (c *Crawler) DownloadWorker(httpClient http.Client, requests chan Request, products chan<- Product, storage Storage, wg *sync.WaitGroup) {
	defer fmt.Printf("DownloadWorker died...\n")
	defer wg.Done()

	for request := range requests {
		var res Response
		var err error

		res, _, err = request.SendRequest(httpClient)
		if err != nil {
			requests <- request
			continue
		}

		fetchedProducts, err := res.GetProducts()
		if err != nil {
			log.Printf("[err] cannot fetch products: %s", err)
			continue
		}

		for _, product := range fetchedProducts {
			products <- product
		}
		storage.PageFinished(c.UniqueID, request.CurrentPage())
	}
}

// SaveWorker func iterates over all fetched products
// and save them to external storage
func (c *Crawler) SaveWorker(products <-chan Product, storage Storage, wg *sync.WaitGroup) {
	defer wg.Done()
	defer fmt.Printf("SaveWorker died...\n")

	for product := range products {
		storage.SaveProduct(c.UniqueID, product)
	}
}

// Start func send details about crawler to Storage
// service
func (c *Crawler) Start(storage Storage) error {
	err := storage.AddCrawler(c.UniqueID, c.MaxPages)
	return err
}

// Finish func send details about crawler to Storage
// service
func (c *Crawler) Finish(storage Storage) error {
	err := storage.FinishCrawler(c.UniqueID)
	return err
}
