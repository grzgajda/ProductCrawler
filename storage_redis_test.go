package main

import (
	"os"
	"testing"
	"time"

	"fmt"

	"github.com/stretchr/testify/assert"
	"gopkg.in/vmihailenco/msgpack.v2"
)

// TestProductCanBePersisted func verif that
// product can be stored in redis and retrieved
// from redis
func TestProductCanBePersistedAndRetrieved(t *testing.T) {
	product := ZanoxProduct{
		ID:   "lorem-ipsum",
		Name: "lorem-ipsum",
	}

	redisHost := os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "localhost:6379"
	}

	redis := CreateRedisClient(redisHost)
	status := redis.Set("test", product, time.Hour)
	if status.Err() != nil {
		t.Fatalf("[err] cannot persist product to redis: %s", status.Err())
		t.FailNow()
	}

	getStatus := redis.Get("test")
	if getStatus.Err() != nil {
		t.Fatalf("[err] cannot retrieve products from redis: %s", getStatus.Err())
		t.FailNow()
	}
	fetchedProduct, err := getStatus.Bytes()
	if err != nil {
		t.Fatalf("[err] cannot retrieve products from redis: %s", getStatus.Err())
		t.FailNow()
	}

	var testTest ZanoxProduct
	err = msgpack.Unmarshal(fetchedProduct, &testTest)
	if err != nil {
		t.Fatalf("[err] cannot decode product from redis: %s", err)
		t.FailNow()
	}

	assert.Equal(t, product.ID, testTest.ID)
	assert.Equal(t, product.Name, testTest.Name)
}

// TestProductCanBeReallyPersisted func tests
// saving real struct of Product to Redis
func TestProductCanBeReallyPersisted(t *testing.T) {
	product := ZanoxProduct{
		ID:   "lorem-ipsum",
		Name: "dolor-sit-amet",
	}

	redisHost := os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "localhost:6379"
	}

	redis := CreateRedisClient(redisHost)
	key, err := redis.SaveProduct("1234", product)
	if err != nil {
		t.Fatalf("[err] cannot persist product to redis: %s", err)
		t.FailNow()
	}

	assert.Equal(t, "product:lorem-ipsum", key)
	productName, err := redis.HGet(key, "name").Result()
	if err != nil {
		t.Fatalf("[err] cannot fetch product's name from redis: %s", err)
		t.FailNow()
	}
	assert.Equal(t, "dolor-sit-amet", productName)
}

// TestRedisPushToListID func tests that new Crawler instance
// has own status stored in Redis instance
func TestRedisPushToListID(t *testing.T) {
	redisHost := os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "localhost:6379"
	}
	redis := CreateRedisClient(redisHost)

	executedCrawlersBefore, err := redis.LLen("crawler:list").Result()
	if err != nil {
		t.Fatalf("[err] cannot fetch amount of executed crawlers: %s", err)
		t.FailNow()
	}

	randomString := GenerateRandomString(64)
	redis.AddCrawler(randomString, 1)

	executedCrawlersAfter, err := redis.LLen("crawler:list").Result()
	if err != nil {
		t.Fatalf("[err] cannot fetch amount of executed crawlers: %s", err)
		t.FailNow()
	}

	lastInsert, err := redis.RPop("crawler:list").Result()
	if err != nil {
		t.Fatalf("[err] cannot pop last element of list: %s", err)
		t.FailNow()
	}

	assert.Equal(t, executedCrawlersBefore+1, executedCrawlersAfter)
	assert.Equal(t, randomString, lastInsert)
}

// TestRedisStartAndFinishCrawler func tests that redis
// properly store information about crawler
func TestRedisStartAndFinishCrawler(t *testing.T) {
	uniqueID := GenerateRandomString(5)

	redisHost := os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "localhost:6379"
	}
	redis := CreateRedisClient(redisHost)

	err := redis.AddCrawler(uniqueID, 10)
	if err != nil {
		t.Fatalf("[err] cannot add new crawler to redis: %s", err)
		t.FailNow()
	}

	err = redis.FinishCrawler(uniqueID)
	if err != nil {
		t.Fatalf("[err] cannot finish existing crawler: %s", err)
		t.FailNow()
	}

	key := fmt.Sprintf("crawler:%s", uniqueID)
	data, err := redis.HGetAll(key).Result()
	if err != nil {
		t.Fatalf("[err] cannot fetch hash from redis: %s", err)
		t.FailNow()
	}

	assert.NotEqual(t, "", data["startTime"])
	assert.NotEqual(t, "", data["finishTime"])
	assert.Equal(t, "10", data["pages"])
}
