package main

import (
	"fmt"
	"net/http"
	"os"
	"sync"

	"time"

	"github.com/urfave/cli"
)

var (
	redisHost    string
	crawlerQuery string

	crawler Crawler
	storage RedisStorage
)

// main func initialize whole app
func main() {
	redisHost = "localhost:6379"

	app := StartApplication()

	// append flags
	app.AppendFlag(CreateRedisFlag())
	app.AppendFlag(CreateQueryFlag())

	// append commands
	app.AppendCommand(CreateZanoxCommand())

	// run application
	app.Run(os.Args)
}

// CreateZanoxCommand func creates command
// for Zanox service
func CreateZanoxCommand() cli.Command {
	command := cli.Command{}
	command.Name = "zanox"
	command.Aliases = []string{"z"}
	command.Usage = "Fetch products from Zanox API"

	// Create basic request and basic crawler
	var basicRequest Request
	var crawler Crawler

	// Create file to store all data about
	// fetched products
	storage = CreateRedisClient(redisHost)

	// execute before base part of command
	command.Before = func(c *cli.Context) error {
		_, err := fmt.Printf("[info] crawler (id: %s, query: %s) session starting...\n", crawler.UniqueID, crawlerQuery)
		if err != nil {
			fmt.Printf("[err] cannot start application without error: %s", err)
			return err
		}

		basicRequest = CreateZanoxRequest()
		basicRequest = basicRequest.ChangeQuery(crawlerQuery)

		crawler, err = CreateCrawler(basicRequest)
		if err != nil {
			fmt.Println("[err] cannot create crawler: %s\n", err)
			return err
		}

		// save status of crawler to redis
		err = crawler.Start(storage)
		return err
	}

	// execute after base part of command
	command.After = func(c *cli.Context) error {
		_, err := fmt.Printf("[info] crawler (id: %s) session finished...\n", crawler.UniqueID)
		if err != nil {
			fmt.Printf("[err] cannot finish application without error: %s", err)
			return err
		}

		// save status of crawler to redis
		err = crawler.Finish(storage)
		return nil
	}

	// base part of command
	command.Action = func(c *cli.Context) error {
		// Create channeles required to communicate
		// between workers
		requests := make(chan Request, crawler.MaxPages)
		products := make(chan Product, crawler.TotalItems)

		// Create basic HTTP client
		httpClient := http.Client{}

		// WaitGroup order application to wait
		// for all workers
		waitGroup := sync.WaitGroup{}

		// Run all download workers to fetch all products
		// from Zanox API
		for i := 0; i < 5; i++ {
			waitGroup.Add(1)
			go crawler.DownloadWorker(httpClient, requests, products, storage, &waitGroup)
		}

		fmt.Printf("[info] fetching %d pages from zanox\n", crawler.MaxPages)
		// Notify about max pages to fetch
		if crawler.MaxPages == 0 {
			return fmt.Errorf("[err] there should be at least 1 page to fetch, is %d", crawler.MaxPages)
		}

		// Send requests to "requests" channel
		for j := 0; j < crawler.MaxPages; j++ {
			if req, err := crawler.Request.ChangePage(j); err == nil {
				select {
				case requests <- req:
					continue
				default:
					time.Sleep(500 * time.Millisecond)
				}
			}
		}

		// Close channel to not send more information
		// to `requests`
		close(requests)

		// Run all save workers to fetch output from
		// requests and persist to external storage
		for k := 0; k < 5; k++ {
			waitGroup.Add(1)
			go crawler.SaveWorker(products, storage, &waitGroup)
		}

		// Waiting for all workers
		waitGroup.Wait()

		// Stop executing action
		return nil
	}

	return command
}

// CreateRedisFlag func create flag
// for Redis hostname
func CreateRedisFlag() cli.Flag {
	flag := cli.StringFlag{}
	flag.Name = "redisHost, rh"
	flag.Value = "localhost:6379"
	flag.Usage = "Define redis hostname"
	flag.Destination = &redisHost

	return flag
}

// CreateQueryFlag func create flag
// for Request's query
func CreateQueryFlag() cli.Flag {
	flag := cli.StringFlag{}
	flag.Name = "query, q"
	flag.Value = ""
	flag.Usage = "Define searching query for products"
	flag.Destination = &crawlerQuery

	return flag
}
