package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"

	"sync"

	"github.com/stretchr/testify/assert"
)

// TestExecutingIsIterable func checks that
// you can define many requests and send them
// together once
func TestExecutingIsIterable(t *testing.T) {
	req := CreateZanoxRequest()
	crw, err := CreateCrawler(req)

	if err != nil {
		t.Fatalf("[err] cannot create crawler: %s", err)
	}

	for i := 1; i < crw.MaxPages; i++ {
		newReq, err := crw.Request.ChangePage(i)
		if err != nil {
			t.Fatalf("[err] cannot switch to another page: %s", err)
		}

		crw.Request = newReq
		url := fmt.Sprintf("http://api.zanox.com/json/2011-03-01/products?connectid=43EEF0445509C7205827&items=50&page=%d&programs=11733", i)
		builtURL, err := crw.Request.BuildURL()

		if err != nil {
			t.Fatalf("[err] cannot build url: %s", err)
			t.FailNow()
		}

		assert.Equal(t, url, builtURL)
	}
}

// TestFetchingProductsFromZanox func checks that
// products are fetching from Zanox API
func TestFetchingProductsFromZanox(t *testing.T) {
	req := CreateZanoxRequest()
	req.Query = "stringi"
	req.Items = 10

	crw, err := CreateCrawler(req)

	if err != nil {
		t.Errorf("[err] cannot create crawler: %s", err)
	}

	requests := make(chan Request, 10)
	products := make(chan Product, 100)

	duration, _ := time.ParseDuration("3s")
	httpClient := http.Client{Timeout: duration}

	redisHost := os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "localhost:6379"
	}
	storage := CreateRedisClient(redisHost)

	wg := sync.WaitGroup{}
	wg.Add(1)

	go crw.DownloadWorker(httpClient, requests, products, storage, &wg)

	for i := 0; i < crw.MaxPages; i++ {
		newReq, err := crw.Request.ChangePage(i)
		if err != nil {
			t.Errorf("[err] cannot change request page: %s", err)
			t.FailNow()
		}

		requests <- newReq
	}

	fetchedProducts := []Product{}

	for j := 0; j < crw.TotalItems; j++ {
		product := <-products
		fetchedProducts = append(fetchedProducts, product)
	}
	close(products)
	close(requests)

	wg.Wait()

	assert.True(t, len(fetchedProducts) > 0)
}

// TestImagesAreDownloadable func testing
// images are downloadable and images is saved
func TestImagesAreDownloadable(t *testing.T) {
	httpClient := http.Client{}
	pwd, err := os.Getwd()

	if err != nil {
		t.Fatalf("[err] cannot get base path directory: %s", err)
		t.FailNow()
	}

	for i, img := range randomImg {
		pathToFile := fmt.Sprintf("%s/tests/img-%d.jpg", pwd, i)

		image := ZanoxImage{img}
		err = DownloadImage(image, httpClient, pathToFile)

		if err != nil {
			t.Fatalf("[err] cannot fetch image: %s", err)
			t.FailNow()
		}
	}

	files, err := ioutil.ReadDir(fmt.Sprintf("%s/tests", pwd))
	if err != nil {
		t.Fatalf("[err] cannot count files in 'tests' directory: %s", err)
	}

	assert.Len(t, files, len(randomImg)+1)
}

// TestEachCrawlerHasDifferentID func tests
// that each UniqueID of crawler is different
func TestEachCrawlerHasDifferentID(t *testing.T) {
	req := CreateZanoxRequest()
	crw1, err1 := CreateCrawler(req)
	crw2, err2 := CreateCrawler(req)

	if err1 != nil || err2 != nil {
		t.Fatalf("[err] cannot create crawlers")
		t.FailNow()
	}

	assert.NotEqual(t, crw1.UniqueID, crw2.UniqueID)
}
