package main

// ZanoxRequest struct allow to
// create requestand parse response
// from JSON to struct
type ZanoxRequest struct {
	Query     string `url:"q,omitempty"`
	Items     int    `url:"items,omitempty"`
	Page      int    `url:"page,omitempty"`
	ConnectID string `url:"connectid"`
	Program   int    `url:"programs"`
}

// ZanoxResponse struct for body of response
// from Zanox API
type ZanoxResponse struct {
	Page         int              `json:"page"`
	Items        int              `json:"items"`
	Total        int              `json:"total"`
	ProductItems ZanoxProductItem `json:"productItems"`
}

// ZanoxProductItem struct contains all products
// fetched from Zanox API
type ZanoxProductItem struct {
	Products []ZanoxProduct `json:"productItem"`
}

// ZanoxProduct struct contains data about single
// product
type ZanoxProduct struct {
	ID              string             `json:"@id"`
	Name            string             `json:"name"`
	Modified        string             `json:"modified"`
	Program         ZanoxProgram       `json:"program"`
	Price           float64            `json:"price"`
	Currency        string             `json:"currency"`
	TrackingLinks   ZanoxTrackingLinks `json:"trackingLinks"`
	Description     string             `json:"description"`
	DescriptionLong string             `json:"descriptionLong"`
	Manufacturer    string             `json:"manufacturer"`
	DeliveryTime    string             `json:"deliveryTime"`
	Image           ZanoxImage         `json:"image"`
	OldPrice        float64            `json:"priceOld"`
	ShippingCosts   int                `json:"shippingCosts"`
	Shipping        int                `json:"shipping"`
	CategoryPath    string             `json:"merchantCategory"`
	CategoryID      int                `json:"merchantProductId"`
}

// ZanoxProgram struct contains data about program
// of product
type ZanoxProgram struct {
	ID   string `json:"@id"`
	Name string `json:"$"`
}

// ZanoxTrackingLinks struct contains all tracking links
// for product
type ZanoxTrackingLinks struct {
	TrackingLink []ZanoxTrackingLink `json:"trackingLink"`
}

// ZanoxTrackingLink struct contains data about tracking links,
// PPC should be URL to product
type ZanoxTrackingLink struct {
	AdSpaceID string `json:"@adspaceId"`
	PPV       string `json:"ppv"`
	PPC       string `json:"ppc"`
}

// ZanoxImage struct contains URL for product's image
type ZanoxImage struct {
	URL string `json:"large"`
}
