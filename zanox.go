package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/go-querystring/query"
	"golang.org/x/text/currency"
	"gopkg.in/vmihailenco/msgpack.v2"
)

var (
	baseURL      = "http://api.zanox.com/json/2011-03-01/products"
	baseCurrency = currency.PLN

	defaultConnectID = "43EEF0445509C7205827"
	defaultPrograms  = 11733
	defaultItems     = 50
)

// CreateZanoxRequest func creates new struct
// for ZanoxRequest with default values
func CreateZanoxRequest() ZanoxRequest {
	return ZanoxRequest{
		Items:     defaultItems,
		ConnectID: defaultConnectID,
		Program:   defaultPrograms,
	}
}

// BuildURL func return a string with url
// to send request
func (req ZanoxRequest) BuildURL() (url string, err error) {
	q, err := query.Values(req)
	if err != nil {
		return url, err
	}

	return fmt.Sprintf("%s?%s", baseURL, q.Encode()), err
}

// SendRequest func create HTTP/GET request
// and wait for response
func (req ZanoxRequest) SendRequest(httpClient http.Client) (Response, Request, error) {
	url, err := req.BuildURL()
	if err != nil {
		return nil, req, err
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, req, err
	}

	response, err := httpClient.Do(request)
	if err != nil {
		return nil, req, err
	}
	if err == nil && response.StatusCode != 200 {
		return nil, req, fmt.Errorf("invalid status code, should be 200, is %d", response.StatusCode)
	}

	var res ZanoxResponse
	err = json.NewDecoder(response.Body).Decode(&res)

	return res, req, nil
}

// ChangePage func sets new value for attribute
// page
func (req ZanoxRequest) ChangePage(page int) (Request, error) {
	if page < 0 {
		return nil, fmt.Errorf("page cannot be lower than 0, is %d", page)
	}

	req.Page = page
	return req, nil
}

// ChangeQuery func sets new value for attribute
// query
func (req ZanoxRequest) ChangeQuery(query string) Request {
	req.Query = query
	return req
}

// ItemsPerPage func return items fetched at most
// by each page
func (req ZanoxRequest) ItemsPerPage() int {
	return req.Items
}

// CurrentPage func return current page value
func (req ZanoxRequest) CurrentPage() int {
	return req.Page
}

// GetProducts func return an array of
// products fetched from request
func (res ZanoxResponse) GetProducts() ([]Product, error) {
	products := []Product{}
	fetchedProducts := res.ProductItems.Products
	if len(fetchedProducts) < 1 {
		return nil, fmt.Errorf("%d products fetched", len(products))
	}

	for _, product := range fetchedProducts {
		products = append(products, product)
	}

	return products, nil
}

// HasNext func checks that next request
// can be send
func (res ZanoxResponse) HasNext() bool {
	return res.Page*res.Items < res.Total
}

// BuildNext func build request struct for
// next HTTP request
func (res ZanoxResponse) BuildNext(req Request) (Request, error) {
	if res.HasNext() == false {
		return req, fmt.Errorf("next request do not exists")
	}

	req.ChangePage(res.Page + 1)
	return req, nil
}

// TotalItems func return integer with all
// possibly products to fetch
func (res ZanoxResponse) TotalItems() int {
	return res.Total
}

// GetID func return product's ID
func (product ZanoxProduct) GetID() string {
	return product.ID
}

// GetName func return product's name
func (product ZanoxProduct) GetName() string {
	return product.Name
}

// GetPrice func return product's price (with currency)
func (product ZanoxProduct) GetPrice() currency.Amount {
	cur, err := currency.ParseISO(product.Currency)
	if err != nil {
		cur = baseCurrency
	}

	return cur.Amount(product.Price)
}

// GetOldPrice func return product's old price (with currency)
func (product ZanoxProduct) GetOldPrice() currency.Amount {
	cur, err := currency.ParseISO(product.Currency)
	if err != nil {
		cur = baseCurrency
	}

	return cur.Amount(product.OldPrice)
}

// GetDescription func return product's description
func (product ZanoxProduct) GetDescription() string {
	return product.Description
}

// GetLongDescription func return product's long description
func (product ZanoxProduct) GetLongDescription() string {
	return product.DescriptionLong
}

// GetImages func return product's images
func (product ZanoxProduct) GetImages() []ProductImage {
	return []ProductImage{
		product.Image,
	}
}

// GetLastModified func return DateTime when product
// was modified last time
func (product ZanoxProduct) GetLastModified() (time.Time, error) {
	parsedTime, err := time.Parse("2006-01-02T15:04:05Z", product.Modified)
	if err != nil {
		return parsedTime, err
	}

	return parsedTime, nil
}

// MarshalBinary func returns the MessagePack encoding
// of product
func (product ZanoxProduct) MarshalBinary() (data []byte, err error) {
	return msgpack.Marshal(product)
}

// GetOriginalURL func return url fetched from API
func (image ZanoxImage) GetOriginalURL() string {
	return image.URL
}
