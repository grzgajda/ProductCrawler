package main

// Storage interface define methods
// required to valid saving products
// to external storage
type Storage interface {
	SaveProduct(uniqueID string, product Product) (string, error)

	AddCrawler(uniqueID string, numberOfPages int) error
	FinishCrawler(uniqueID string) error
	PageFinished(uniqueID string, page int) error
}
