package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

var (
	randomImg = []string{
		"http://cdn.so.fantasti.cc/big/b/i/s/bisexualguy/bisexualguy_4db252.jpg",
		"http://www.babesandhotties.com/wp-content/uploads/2014/11/4146-Stunning-model-with-nice-tits-and-pale-curvaceous-body.jpg",
		"https://www.completeporndatabase.com/sp/01/1418139211-kin-asian-sexy-model-horny-babe-big-tits-huge-tits-640.jpg",
		"https://s9v7j7a4.ssl.hwcdn.net/galleries/new_big/db/5f/1a/db5f1a9f5f8f79f31a068bc8036deaf3/6.jpg",
	}
)

// TestRequestBuildsProperUrl func testing that
// query built from method is valid
func TestRequestBuildsProperUrl(t *testing.T) {
	req := ZanoxRequest{
		Query:     "lorem",
		ConnectID: "ipsum",
		Program:   123,
	}

	url, err := req.BuildURL()
	if err != nil {
		t.Fatalf("[fail] cannot build url: %s", err)
		t.FailNow()
	}

	assert.Equal(
		t,
		"http://api.zanox.com/json/2011-03-01/products?connectid=ipsum&programs=123&q=lorem",
		url,
	)
}

// TestSwitchingPagesWorksProper func testing that
// method ChangePage return valid new Request
func TestSwitchingPagesWorksProper(t *testing.T) {
	req := ZanoxRequest{
		Query:     "lorem",
		ConnectID: "ipsum",
		Program:   123,
	}

	newReq, err := req.ChangePage(3)
	if err != nil {
		t.Fatalf("[fail] invalid page site: %s", err)
	}

	url, err := newReq.BuildURL()
	if err != nil {
		t.Fatalf("[fail] cannot build url: %s", err)
		t.FailNow()
	}

	assert.Equal(
		t,
		"http://api.zanox.com/json/2011-03-01/products?connectid=ipsum&page=3&programs=123&q=lorem",
		url,
	)
}

// TestProductImplementsBasicMethod func testing
// basic implementation of Product interface
func TestProductImplementsBasicMethod(t *testing.T) {
	product := ZanoxProduct{
		ID:       "lorem",
		Name:     "ipsum",
		Price:    32.99,
		Currency: "EUR",
		OldPrice: 0,
		Image:    ZanoxImage{"http://www.onet.pl"},
	}

	cur, err := currency.ParseISO("EUR")
	if err != nil {
		t.Fatalf("[err] cannot create PLN currency: %s", err)
		t.FailNow()
	}

	assert.Equal(t, "lorem", product.GetID())
	assert.Equal(t, "ipsum", product.GetName())
	assert.Equal(t, cur.Amount(product.Price), product.GetPrice())
	assert.Equal(t, cur.Amount(product.OldPrice), product.GetOldPrice())
	assert.Len(t, product.GetImages(), 1)
}

// TestProductConvertTimeValid func testing
// correct mapping from string to time.Time
func TestProductConvertTimeValid(t *testing.T) {
	product := ZanoxProduct{
		Modified: "2017-03-05T02:02:05Z",
	}
	modified, err := product.GetLastModified()

	if err != nil {
		t.Fatalf("[err] cannot convert string to time.Time: %s", err)
		t.FailNow()
	}

	assert.Equal(t, 2017, modified.Year())
	assert.Equal(t, "March", modified.Month().String())
	assert.Equal(t, 5, modified.Day())
}

// TestProductConvertPriceValid func tests
// proper mapping currency.Amount to string
func TestProductConvertPriceValid(t *testing.T) {
	product := ZanoxProduct{
		Currency: "EUR",
		Price:    16.99,
	}

	price := product.GetPrice()
	printer := message.NewPrinter(language.Polish)
	formatter := currency.ISO(price)

	assert.Equal(t, "EUR", price.Currency().String())
	assert.Equal(t, "EUR 16.99", printer.Sprint(formatter))
}
