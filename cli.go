package main

import (
	"github.com/urfave/cli"
)

var (
	appName        = "Products Scrapper"
	appUsage       = "Fetch products!"
	appDescription = "Simple CLI app to fetch products from different API sources"
	appVersion     = "0.1.0"
)

// App struct is basic wrapper for cli.App
type App struct {
	*cli.App
}

// AppendCommand func attach new command to commands
// list
func (app App) AppendCommand(command cli.Command) {
	app.Commands = append(app.Commands, command)
}

// AppendFlag func attach new flag to flags list
func (app App) AppendFlag(flag cli.Flag) {
	app.Flags = append(app.Flags, flag)
}

// StartApplication func return a new instance
// of cli.App with preconfigured values
func StartApplication() App {
	app := cli.NewApp()
	app.EnableBashCompletion = true
	app.Authors = []cli.Author{
		cli.Author{
			Name:  "Grzegorz Gajda",
			Email: "grz.gajda@outlook.com",
		},
	}
	app.Version = appVersion
	app.Name = appName
	app.Usage = appUsage
	app.Description = appDescription

	return App{app}
}
