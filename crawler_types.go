package main

import (
	"encoding"
	"net/http"
	"time"

	"golang.org/x/text/currency"
)

// Request interface define built-in
// methods for request
type Request interface {
	BuildURL() (url string, err error)
	SendRequest(httpClient http.Client) (res Response, next Request, err error)
	ChangePage(page int) (Request, error)
	ChangeQuery(query string) Request
	ItemsPerPage() int
	CurrentPage() int
}

// Response interface define built-in
// methods for response
type Response interface {
	GetProducts() ([]Product, error)
	TotalItems() int
	HasNext() bool
	BuildNext(req Request) (Request, error)
}

// Product interface define built-in
// methods for product
type Product interface {
	encoding.BinaryMarshaler
	GetID() string
	GetName() string
	GetPrice() currency.Amount
	GetOldPrice() currency.Amount
	GetDescription() string
	GetLongDescription() string
	GetImages() []ProductImage
	GetLastModified() (time.Time, error)
}

// ProductImage interface define
// built-in methods for image
type ProductImage interface {
	GetOriginalURL() string
}
