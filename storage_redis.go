package main

import (
	"fmt"

	"time"

	"gopkg.in/redis.v5"
)

// RedisStorage struct is simple wrapper
// for redis.Client struct
type RedisStorage struct {
	*redis.Client
}

// CreateRedisClient func return new instance
// of RedisStorage
func CreateRedisClient(redisHost string) RedisStorage {
	client := redis.NewClient(&redis.Options{
		Addr:     redisHost,
		Password: "",
		DB:       0,
	})

	return RedisStorage{client}
}

// SaveProduct func automatically create (or update) new
// Redis's hash with values from product
func (storage RedisStorage) SaveProduct(uniqueID string, product Product) (string, error) {
	// Generate unique key for product
	key := fmt.Sprintf("product:%s", product.GetID())

	// Set ID attr
	res := storage.HSet(key, "id", product.GetID())
	if res.Err() != nil {
		return "", res.Err()
	}

	// Set Name attr
	res = storage.HSet(key, "name", product.GetName())
	if res.Err() != nil {
		return "", res.Err()
	}

	// Set Description attr
	res = storage.HSet(key, "description", product.GetDescription())
	if res.Err() != nil {
		return "", res.Err()
	}

	// Set LongDescription attr
	res = storage.HSet(key, "longDescription", product.GetLongDescription())
	if res.Err() != nil {
		return "", res.Err()
	}

	// Set LastModified attr
	if lastModified, err := product.GetLastModified(); err == nil {
		res = storage.HSet(key, "lastModified", lastModified.String())
		if res.Err() != nil {
			return "", res.Err()
		}
	}

	// Add value to HyperLogLogs for statistics
	// purpose
	productsKey := fmt.Sprintf("crawler:%s:products", uniqueID)
	intCmd := storage.SAdd(productsKey, product.GetID())
	if intCmd.Err() != nil {
		return key, intCmd.Err()
	}

	return key, nil
}

// PageFinished func set status true for current crawler
// action and current page
func (storage RedisStorage) PageFinished(uniqueID string, page int) error {
	// Generate key for page and convert int to string
	key := fmt.Sprintf("crawler:%s:pages", uniqueID)
	pageString := fmt.Sprintf("%d", page)

	// Set status for single page
	res := storage.HSet(key, pageString, true)
	if res.Err() != nil {
		return res.Err()
	}

	// Generate key for HyperLogLogs and add counter
	pfKey := fmt.Sprintf("crawler:%s:pages:count", uniqueID)
	intRes := storage.PFAdd(pfKey, page)

	return intRes.Err()
}

// AddCrawler func add crawler's UniqueID to list of crawlers
func (storage RedisStorage) AddCrawler(uniqueID string, numberOfPages int) error {
	status := storage.RPush("crawler:list", uniqueID)
	if status.Err() != nil {
		return status.Err()
	}

	key := fmt.Sprintf("crawler:%s", uniqueID)

	currentTime := time.Now()
	status1 := storage.HSet(key, "startTime", currentTime.String())
	status2 := storage.HSet(key, "pages", numberOfPages)

	if status1.Err() != nil || status2.Err() != nil {
		return fmt.Errorf("cannot set value for key 'startTime' or 'pages'")
	}

	return nil
}

// FinishCrawler func notify redis that crawler
// finished job
func (storage RedisStorage) FinishCrawler(uniqueID string) error {
	key := fmt.Sprintf("crawler:%s", uniqueID)
	status := storage.HSet(key, "finishTime", time.Now().String())

	return status.Err()
}
