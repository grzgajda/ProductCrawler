package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
)

// TestStartApplicationIsPreconfigured func checks
// basic values for new cli.App instance
func TestStartApplicationIsPreconfigured(t *testing.T) {
	app := StartApplication()

	assert.Equal(t, appName, app.Name)
	assert.Equal(t, appUsage, app.Usage)
	assert.Equal(t, appDescription, app.Description)
	assert.Equal(t, appVersion, app.Version)
}

// TestCommandIsAppending func checks that append
// works properly for commands
func TestCommandIsAppending(t *testing.T) {
	app := StartApplication()
	assert.Len(t, app.Commands, 0)

	command := cli.Command{}
	app.AppendCommand(command)

	assert.Len(t, app.Commands, 1)
}

// TestFlagIsAppending func checks that append
// works properly for flags
func TestFlagIsAppending(t *testing.T) {
	app := StartApplication()
	assert.Len(t, app.Flags, 0)

	flag := cli.StringFlag{}
	app.AppendFlag(flag)

	assert.Len(t, app.Flags, 1)
}
