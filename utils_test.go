package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestStringsAreDifferent tests that func
// GenerateRandomString always return different
// string with fixed length
func TestStringsAreDifferent(t *testing.T) {
	string1 := GenerateRandomString(10)
	string2 := GenerateRandomString(10)
	string3 := GenerateRandomString(10)

	assert.Len(t, string1, 10)
	assert.Len(t, string2, 10)
	assert.Len(t, string3, 10)

	assert.NotEqual(t, string1, string2)
	assert.NotEqual(t, string2, string3)
	assert.NotEqual(t, string3, string1)
}
